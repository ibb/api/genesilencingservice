package de.unigoettingen.ibeetlebase.api;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeGeneSilencingSequenceResourceIT extends GeneSilencingSequenceResourceTest {

    // Execute the same tests but in native mode.
}