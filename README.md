# Gene Silencing Service for the iBeetle-Base project

## Running the application in dev mode

```
./mvnw quarkus:dev
```

## Packaging and containerizing

The application can be packaged and containerized by one command (docker required)

```
./mvnw package
```

