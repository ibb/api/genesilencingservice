package ibb.api.genesilencing.util;

import static java.util.stream.Collectors.toCollection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.jboss.logging.Logger;

public class Util {
    
    private static final Logger LOG = Logger.getLogger(Util.class);
    
    public static <C extends Collection<String>> C trimAndSplit(String str, Supplier<C> collectionFactory) {
        return Arrays.stream(str.trim().split(","))
                .map(String::trim)
                .filter(Predicate.not(""::equals))
                .collect(toCollection(collectionFactory));
    }
    
    public static Stream<CSVRecord> parseCSVFile(String URIString, String ...headers) throws IOException {
        URL url = getURL(URIString);
        if (url == null) {
            return Stream.empty();
        }
        
        return StreamSupport.stream(getCSVParser(url, headers).spliterator(), false);
    }

    private static CSVParser getCSVParser(URL url, String[] headers) throws IOException {
        return new CSVParser(new BufferedReader(new InputStreamReader(url.openStream())),
                CSVFormat.DEFAULT
                    .withHeader(headers)
                    .withSkipHeaderRecord()
                    .withDelimiter(',')
                    .withTrim());
    }

    private static URL getURL(String str) {
        try {
            URI uri = new URI(str);
            if (!uri.isAbsolute()) {
                uri = Paths.get(str).toAbsolutePath().toUri();
            }
            return uri.toURL();
        } catch (URISyntaxException | IllegalArgumentException | MalformedURLException e) {
            LOG.errorv("Bad URL {1}.", str);
        }
        return null;
    }
}
