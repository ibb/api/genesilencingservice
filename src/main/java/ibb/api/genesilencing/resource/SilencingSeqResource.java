package ibb.api.genesilencing.resource;

import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameters;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;

import ibb.api.genesilencing.model.SilencingSeq;
import ibb.api.genesilencing.repo.SilencingSeqRepo;
import ibb.api.genesilencing.util.Util;

@Path("/silencingseqs")
@Produces(MediaType.APPLICATION_JSON)
public class SilencingSeqResource {

    @Inject
    SilencingSeqRepo repo;
    
    @GET
    @Operation(summary = "Find a list of silencing sequences (iB fragments)")
    @Parameters({
        @Parameter(name = "geneIds",
                description = "Find sequences that match these genes (comma-separated). Should not be used together with param \"ids\"", 
                example = "TC000292,TC000021"),
        @Parameter(name = "ids",
                description = "Find sequences with these ids (comma-separated). Should not be used together with param \"geneIds\"",
                example = "iB_00061,iB_00010")
    })
    @APIResponses({
        @APIResponse(responseCode = "200", description = "Success"),
        @APIResponse(responseCode = "400", description = "Bad request")
    })
    public List<SilencingSeq> list(
            @DefaultValue("") @QueryParam("geneIds") String geneIds,
            @DefaultValue("") @QueryParam("ids") String ids) {

        Set<String> idSet = Util.trimAndSplit(ids, HashSet::new);
        Set<String> geneIdSet = Util.trimAndSplit(geneIds, HashSet::new);
        
        if (idSet.size() > 0 && geneIdSet.size() > 0) {
            throw new BadRequestException("The params \"ids\" and \"geneIds\" should not be used together");
        }
        
        Stream<SilencingSeq> results;
        if (geneIdSet.size() > 0) {
            results = geneIdSet.stream()
                    .map(repo::getByGeneId)
                    .flatMap(Collection::stream)
                    .distinct();
        } else if (idSet.size() > 0) {
            results = idSet.stream()
                    .map(repo::get)
                    .filter(Optional::isPresent)
                    .map(Optional::get);
        } else {
            results = repo.get().stream();
        }
        return results.collect(toList());
    }

    @GET
    @Path("/{id}")
    @Operation(summary = "Find information for a silencing sequence (an iB fragment).")
    @Parameters({
        @Parameter(name = "id", description = "iB number in format iB_[0-9]{5}", example = "iB_00061")
    })
    @APIResponses({
        @APIResponse(responseCode = "200", description = "Success"),
        @APIResponse(responseCode = "404", description = "No sequence found")
    })
    public SilencingSeq get(@PathParam("id") String id) {
        return repo.get(id).orElseThrow(NotFoundException::new);
    }
}
