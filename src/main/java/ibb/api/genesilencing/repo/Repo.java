package ibb.api.genesilencing.repo;

import java.util.Collection;
import java.util.Optional;

public interface Repo<T> {
    Optional<T> get(String id);
    Collection<T> get();
}
