package de.unigoettingen.ibeetlebase.api;

import org.junit.jupiter.api.Test;

import ibb.api.genesilencing.resource.SilencingSeqResource;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestHTTPEndpoint(SilencingSeqResource.class)
public class GeneSilencingSequenceResourceTest {

    @Test
    public void testListEndpoint() {
        //given().queryParam("geneIds", "TC000021").when().get().then().statusCode(200);
    }

    @Test
    public void testGetEndpoint() {
        //given().pathParam("id", "iB_00002").when().get("/{id}").then().statusCode(204);
    }
}