package ibb.api.genesilencing.repo;

import java.util.Collection;

import ibb.api.genesilencing.model.SilencingSeq;

public interface SilencingSeqRepo extends Repo<SilencingSeq> {
    Collection<SilencingSeq> getByGeneId(String geneId);
}
