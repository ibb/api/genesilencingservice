package ibb.api.genesilencing.model;

import static java.util.Collections.emptySet;

import java.util.Set;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class SilencingSeq {
    public String id;
    public Set<String> geneIds;
    public String seq;
    public String leftPrimer;
    public String rightPrimer;
    
    public SilencingSeq(Builder builder) {
        id = builder.id;
        geneIds = builder.geneIds;
        seq = builder.seq;
        leftPrimer = builder.leftPrimer;
        rightPrimer = builder.rightPrimer;
    }
    
    public String getId() {
        return id;
    }

    public Set<String> getGeneIds() {
        return geneIds == null ? emptySet() : geneIds;
    }

    public String getSeq() {
        return seq;
    }

    public String getLeftPrimer() {
        return leftPrimer;
    }

    public String getRightPrimer() {
        return rightPrimer;
    }

    public static class Builder {

        public String id;
        public Set<String> geneIds;
        public String seq;
        public String leftPrimer;
        public String rightPrimer;
        
        public Builder(String id) {
            this.id = id;
        }
        
        public Builder geneIds(Set<String> vals) {
            geneIds = vals;
            return this;
        }
        
        public Builder seq(String val) {
            seq = val;
            return this;
        }
        
        public Builder leftPrimer(String val) {
            leftPrimer = val;
            return this;
        }
        
        public Builder rightPrimer(String val) {
            rightPrimer = val;
            return this;
        }
        
        public SilencingSeq build() {
            return new SilencingSeq(this);
        }
    }
}
