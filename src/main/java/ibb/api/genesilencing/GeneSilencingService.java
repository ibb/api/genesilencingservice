package ibb.api.genesilencing;


import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(title = "Gene Silencing Service",
                description = "Get information for silencing sequences (iB fragments).",
                version = "1.0")
)
public class GeneSilencingService extends Application {

}
