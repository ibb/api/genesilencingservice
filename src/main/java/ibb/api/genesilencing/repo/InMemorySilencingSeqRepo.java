package ibb.api.genesilencing.repo;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.csv.CSVRecord;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import ibb.api.genesilencing.model.SilencingSeq;
import ibb.api.genesilencing.util.Util;
import io.quarkus.runtime.Startup;

@Startup
@ApplicationScoped
public class InMemorySilencingSeqRepo implements SilencingSeqRepo {
    
    private static final Logger LOG = Logger.getLogger(InMemorySilencingSeqRepo.class);
    
    @ConfigProperty(name = "data.genemap.uri")
    Optional<String> geneMapURI;
    
    @ConfigProperty(name = "data.sequence.uri")
    Optional<String> sequenceURI;

    private Map<String, SilencingSeq> index;
    private Map<String, Collection<SilencingSeq>> geneIdIndex;

    @Override
    public Optional<SilencingSeq> get(String id) {
        return Optional.ofNullable(index.get(id));
    }

    @Override
    public Collection<SilencingSeq> get() {
        return index.values();
    }

    @Override
    public Collection<SilencingSeq> getByGeneId(String geneId) {
        return geneIdIndex.getOrDefault(geneId, Collections.emptyList());
    }

    @PostConstruct
    void init() {
        prepareData();
    }
    
    /**
     * Load data from files and create in-memory indexes
     */
    private void prepareData() {
        index = sequenceURI.map(uri -> loadData(uri)).orElseGet(() -> {
            LOG.info("No URI provided for sequence data");
            return new HashMap<>();
        });
        
        geneIdIndex = index.values()
                .stream()
                .flatMap(seq -> seq.getGeneIds()
                        .stream()
                        .map(geneId -> new SimpleEntry<String, SilencingSeq>(geneId, seq)))
                .collect(groupingBy(SimpleEntry::getKey,
                        mapping(SimpleEntry::getValue, toCollection(ArrayList::new))));
    }
    
    private Map<String, SilencingSeq> loadData(String uri) {
        Map<String, Set<String>> geneMap = geneMapURI.map(u -> loadGeneMap(u)).orElseGet(() -> {
            LOG.info("No URI provided for gene mappings");
            return new HashMap<>();
        });
        
        try (Stream<CSVRecord> records = Util.parseCSVFile(uri,
                "id",
                "seq",
                "leftPrimer",
                "rightPrimer")) {
            LOG.infov("Loading sequence data from {0}", uri);
            
            Map<String, SilencingSeq> map = records.map(r -> new SilencingSeq.Builder(r.get("id"))
                        .seq(r.get("seq"))
                        .leftPrimer(r.get("leftPrimer"))
                        .rightPrimer(r.get("rightPrimer"))
                        .geneIds(geneMap.get(r.get("id")))
                        .build())
                    .collect(Collectors.toMap(SilencingSeq::getId, Function.identity()));
            
            // Check silencing sequences with no associated genes or no sequence data
            Set<String> withGeneSet = geneMap.keySet();
            Set<String> withSeqSet = map.keySet();
            Set<String> withSeqWithoutGeneSet = withSeqSet.stream().filter(k -> map.get(k).getGeneIds().isEmpty()).collect(toSet());
            Set<String> withGeneWithoutSeqSet = withGeneSet.stream().filter(k -> !withSeqSet.contains(k)).collect(toSet());
            
            if (withSeqWithoutGeneSet.size() > 0) {
                LOG.warnv("{0} silencing sequences are not associated with any gene (E.g. {1})",
                        withSeqWithoutGeneSet.size(),
                        String.join(", ", withSeqWithoutGeneSet.stream()
                            .limit(3)
                            .collect(toList())));
            }
            if (withGeneWithoutSeqSet.size() > 0) {
                LOG.warnv("{0} silencing sequences do not have sequence data (E.g. {1})",
                        withGeneWithoutSeqSet.size(),
                        String.join(", ", withGeneWithoutSeqSet.stream()
                            .limit(3)
                            .collect(toList())));
                
                // Also include silencing sequences without sequence data in the service
                withGeneWithoutSeqSet.stream().forEach(k -> {
                   map.put(k, new SilencingSeq.Builder(k)
                           .geneIds(geneMap.get(k))
                           .build()); 
                });
            }
            
            LOG.infov("{0} silencing sequences are loaded in total", map.size());
            
            return map;
            
        } catch (IOException | IllegalArgumentException | IllegalStateException e) {
            LOG.errorv("Fail to parse sequence data from {0}: {1}", uri, e.getMessage());
        }
        return new HashMap<>();
    }
    
    private Map<String, Set<String>> loadGeneMap(String uri) {
        try (Stream<CSVRecord> records = Util.parseCSVFile(uri,
                "id",
                "geneId")) {
            LOG.infov("Loading gene mappings from {0}", uri);
            
            return records
                    .collect(groupingBy(r -> r.get("id"),  
                            mapping(r -> r.get("geneId"), Collectors.toSet())));
            
        } catch (IOException | IllegalArgumentException | IllegalStateException e) {
            LOG.errorv("Fail to parse gene mappings from {0}: {1}", uri, e.getMessage());
        }
        return new HashMap<>();
    }
}
